# Web Development Assignment: HTML/CSS Form

## Introduction

Welcome to the first step in your web development journey! In this assignment, you will be creating a simple HTML/CSS form. This assignment aims to solidify your understanding of HTML structure, CSS styling, and form elements.

## Objective

The objective of this assignment is to create a basic form using HTML and style it using CSS. By completing this assignment, you will gain practical experience in structuring HTML documents, incorporating form elements, and styling them with CSS.

## Instructions

Follow these steps to complete the assignment:

1. **Set Up Your Environment**

   - Ensure you have a text editor installed on your computer, such as Visual Studio Code, Sublime Text, or Atom.
   - Create a new folder for this assignment and open it in your preferred text editor.

2. **Create the HTML File**

   - Inside your project folder, create a new file named `index.html`.
   - Open `index.html` in your text editor.
   - Start by creating the basic structure of an HTML document using the `<!DOCTYPE html>` declaration and `<html>`, `<head>`, and `<body>` tags.

3. **Design Your Form**

   - Within the `<body>` tags, create a `<form>` element to encapsulate your form.
   - Add various form elements such as text inputs, checkboxes, radio buttons, dropdown menus, etc., depending on the requirements of your form.
   - Use appropriate HTML attributes like `name`, `id`, and `placeholder` to label and identify your form elements.

4. **Style Your Form with CSS**

   - Create a new file named `styles.css` in your project folder.
   - Link the `styles.css` file to your HTML document using the `<link>` tag inside the `<head>` section.
   - Write CSS rules to style your form elements. Experiment with properties like `padding`, `margin`, `font-size`, `background-color`, `border`, etc., to achieve your desired look and feel.

5. **Test Your Form**

   - Open your `index.html` file in a web browser to see how your form looks.
   - Make any necessary adjustments to your HTML and CSS code to refine the appearance and functionality of your form.
   - Test different form inputs to ensure they behave as expected.

6. **Submit Your Assignment**
   - Once you're satisfied with your form, share your `index.html` and `styles.css` files with your tutor for review.

## Resources

Here are some resources to help you complete this assignment:

- [W3Schools HTML Forms Tutorial](https://www.w3schools.com/html/html_forms.asp)
- [MDN Web Docs: CSS](https://developer.mozilla.org/en-US/docs/Web/CSS)
- [CSS Tricks: A Complete Guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

## Conclusion

Congratulations on completing the first step of your web development journey! By creating this HTML/CSS form, you've taken a significant step towards mastering the fundamentals of web development. Stay tuned for more assignments to further enhance your skills. Happy coding!
